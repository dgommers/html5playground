#
#	HTML5PLAYGROUND
#	Avans Hogeschool Breda
#	VH8IB3
#


View online
	http://derkgommers.nl/exp/html5playground/

Source
	https://bitbucket.org/dgommers/html5playground/src

Documentation
	https://bitbucket.org/dgommers/html5playground/downloads

Authors
	Derk Gommers
	Rens Vermeulen
	Carlos Yap
	Niels de Wijs
	Tim Lokers