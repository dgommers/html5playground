<?php

	$menu = array();

	function amenu ($name, $title) {
		global $menu;
		$str = '<li';

		if (isset($_GET['p']) && $_GET['p'] == $name) {
			$str .= ' class="active"';
		} else if (!isset($_GET['p']) && $name == '') {
			$str .= ' class="active"';
		}
		$str .= '><a href="index.php?p='.$name.'">'.$title.'</a></li>';
		$menu[] = $name;
		return $str;
	}

	function template () {
		global $menu;
		return isset($_GET['p']) && !empty($_GET['p']) && in_array($_GET['p'], $menu) ? $_GET['p'] : 'storage';
	}

	$m = amenu('storage', 'Storage').amenu('applicationcache', 'App cache').amenu('webworkers', 'Web Workers').amenu('sse', 'Server-sent events');
	$t = template();
?>
<!doctype html>
<html manifest="appcache.php">
<head>
	<title>Playground</title>
	<link rel="stylesheet" href="lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="lib/css/style.css">
</head>
<body id="<?=$t?>">
	<div class="container">
		<div class="navbar navbar-inverse">
			<div class="navbar-inner">
				<div class="container">
					<a class="brand" href=".">Playground</a>
					<ul class="nav">
						<?=$m?>
					</ul>
					<a id="onlineBtn" class="btn btn-success pull-right btn-disabled">Online</a>
				</div>
			</div>
		</div>
		<?php
			require 'php/'.$t.'.php';
		?>
	</div>

	<script src="lib/js/ext.jquery.js"></script>
	<script src="lib/js/ext.chart.min.js"></script>
	<script src="lib/js/ext.bootstrap.min.js"></script>
	<script src="lib/js/ext.benchmark.js"></script>
	<script src="lib/js/play.appcache.js"></script>
	<script src="lib/js/play.benchmark.js"></script>
	<script src="lib/js/play.test.js"></script>
	<script src="lib/js/play.events.js"></script>
	<script src="lib/js/webworker.js"></script>
</body>
</html>