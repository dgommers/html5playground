var play = play || {};

play.appcache = (function () {

	var elem, cons, updateBtn, reloadBtn;

	function logEvent (event) {
		var date = new Date();
		cons.append(
			('0'+date.getHours()).slice(-2)+":"
			+('0'+date.getMinutes()).slice(-2)+":"
			+('0'+date.getSeconds()).slice(-2)+" > "
			+event.type+"\n");	 

		if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
			window.applicationCache.swapCache();
			  
			cons.append(('0'+date.getHours()).slice(-2)+":"
			+('0'+date.getMinutes()).slice(-2)+":"
			+('0'+date.getSeconds()).slice(-2)+" > "
			+"reload"+"\n");

			updateBtn.addClass('disabled').removeClass('btn-primary');
			reloadBtn.removeClass('disabled').addClass('btn-primary');
		}  
		cons.get(0).scrollTop = 9E9;
	}

	function checkForUpdate (event) {
		event.preventDefault();
		if (!updateBtn.hasClass('disabled')) {
			window.applicationCache.update();
		}
	}

	return {
		init: function () {
			elem = $('#appcache');
			updateBtn = elem.find('#updateBtn'),
			reloadBtn = elem.find('#reloadBtn'),
			cons = elem.find('#console');

			updateBtn.on('click', checkForUpdate);
			reloadBtn.on('click', function (event) {
				event.preventDefault();
				if (!reloadBtn.hasClass('disabled')) {
					window.location.reload(true); 
				}
			});

			window.applicationCache.addEventListener('checking',logEvent,false);
			window.applicationCache.addEventListener('noupdate',logEvent,false);
			window.applicationCache.addEventListener('downloading',logEvent,false);
			window.applicationCache.addEventListener('cached',logEvent,false);
			window.applicationCache.addEventListener('updateready',logEvent,false);
			window.applicationCache.addEventListener('obsolete',logEvent,false);
			window.applicationCache.addEventListener('error',logEvent,false);
		}
	} 
})();