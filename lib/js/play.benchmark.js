var play = play || {};
play.benchmark = (function () {

	var ICON_OK = '<i class="icon-ok"></i>',
		ICON_PLAY = '<i class="icon-play"></i>',
		ICON_PENDING = '<i class="icon-pause"></i>',
		ICON_CACHE = '<i class="icon-time"></i>';

	var elem, rows, btn, suite, 
		tests = [],
		opt = {
			async: true,
			maxTime: 5
		};

	return {
		init: function () {
			elem = $('#benchmark');
			rows = elem.find('tbody tr');
			btn = elem.find('.btn');
			suite = new Benchmark.Suite;

			this.register();
			this.bind();
		},
		register: function () {
			for (var i = 0; i < rows.length; i++) {
				var row = rows.eq(i),
					test = row.attr('data-test'),
					title = row.find('td').eq(0).text(),
					cache = this.open(test);

				if (play.test[test] != undefined) {
					play.test[test].owner = test;
					tests.push(play.test[test]);
					suite.add(title, play.test[test].run);

					if (cache) {
						this.result(i, cache.time, cache.hz, cache.rme, cache.samples, ICON_CACHE);
					}

				} else {
					this.printRow(i, 'Cannot find test '+test);
				}	
			}
		},
		cycle: function (index, test) {
			var obj = {
				time: test.times.elapsed,
				hz: test.hz,
				rme: test.stats.rme,
				samples: test.stats.sample.length
			};
			this.result(index, obj.time, obj.hz, obj.rme, obj.samples, ICON_OK);	
			this.save(tests[index].owner, obj);
			if (index < rows.length-1) {
				this.emptyRow(index+1, ICON_PLAY);
			}
		},
		save: function (name, values) {
			localStorage.setItem('benchmark.'+name, JSON.stringify(values));
		},
		open: function (name) {
			var result = localStorage.getItem('benchmark.'+name);
			if (result != undefined) {
				return JSON.parse(result);
			} else {
				return false;
			}
		},
		result: function (index, time, hz, rme, samples, icon) {
			time = time+' s';
			hz = Benchmark.formatNumber(hz.toFixed(hz < 100 ? 2 : 0))+' ops';
			rme = '\xb1'+rme.toFixed(2)+'%',

			this.printRow(index, time, hz, rme, icon);	
		},
		start: function () {
			for (var i = 0; i < rows.length; i++) {
				this.emptyRow(i, i == 0 ? ICON_PLAY : ICON_PENDING);
			}
		},
		complete: function () {
			for (var i = 0; i < tests.length; i++) {
				tests[i].teardown();
			}
		},
		bind: function () {
			// .name x .hz ops/sec ±.stats.rme% (stats.sample.length runs sampled) 
			var me = this;
			suite.on('cycle', function (event) {
				me.cycle(event.target.id-1, event.target);
			});
			suite.on('start', function () {
				me.start();
			});
			suite.on('complete', function () {
				me.complete();
			});
			btn.on('click', function (event) {
				event.preventDefault();
				for (var i = 0; i < tests.length; i++) {
					tests[i].setup();
				}
				suite.run(opt);
			});
		},
		emptyRow: function (row, icon) {
			this.printRow(row, '', '', '', icon ? icon : '');
		},
		printRow: function (row) {
			var cells = rows.eq(row).find('td');
			for (var i = 1; i < arguments.length; i++) {
				cells.eq(i).html(arguments[i]);
			}
		}
	};

})();