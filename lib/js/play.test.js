var play = play || {},
	STRING_128BYTES = 'This string should contain 128 characters, if so, it will be around 128 bytes. We need these bytes to create a performance test.';
	STRING_512BYTES = STRING_128BYTES+STRING_128BYTES+STRING_128BYTES+STRING_128BYTES;

play.test = {
	localstorage: {
		setup: function () { },
		run: function () {
			localStorage.setItem('t', STRING_512BYTES);
			localStorage.getItem('t');
		},
		teardown: function () {
			localStorage.removeItem('t');
		}
	},
	sessionstorage: {
		setup: function () { },
		run: function () {
			sessionStorage.setItem('t', STRING_512BYTES);
			sessionStorage.getItem('t');
		},
		teardown: function () {
			sessionStorage.removeItem('t');
		}
	},
	cookie: {
		setup: function () { 
       		var date = new Date();
       		date.setTime(date.getTime() + (20 * 24 * 60 * 60 * 1000));
        	play.test.cookie.expires = '; expires=' + date.toGMTString();
        	
			date = undefined;
		},
		run: function () {
			document.cookie = 't='+STRING_512BYTES + play.test.cookie.expires + '; path=/';	
		},
		teardown: function () { 
			document.cookie = '';
		}
	},
	websql: {
		setup: function () {
			play.test.websql.database = openDatabase('test', '1.0', 'Test DB', 1);
			play.test.websql.database.transaction(function (tx) {
			   tx.executeSql('CREATE TABLE IF NOT EXISTS test (id unique, value);');
			});
		},
		run: function () {
			play.test.websql.database.transaction(function (tx) {
			   tx.executeSql('INSERT INTO test (id, value) VALUES (1, \''+STRING_512BYTES+'\')');
			   tx.executeSql('SELECT * FROM test WHERE id=1 LIMIT 1');
			   tx.executeSql('DELETE FROM test WHERE id=1 LIMIT 1');
			});
		},
		teardown: function () {
			play.test.websql.database = undefined;
		}
	},
	remoteapi: {
		setup: function () { },
		run: function () {
			var x = $.ajax({
			    type: 'GET',
			    url: window.location.href,
			    dataType: 'json',
			    success: function() { },
			    data: {t:STRING_512BYTES},
			    async: false
			}).responseText;
			x = undefined;	
		},
		teardown: function () { }
	}
}