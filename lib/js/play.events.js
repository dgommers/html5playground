var doc = $(document),
	play = play || {};

play.events = (function () {

	var online, onlineBtn, o = {};
 
	function updateOnline () {
		if (online != navigator.onLine) {
			online = navigator.onLine;
			if (online) {
				onlineBtn.removeClass('btn-error').addClass('btn-success').text('Online');
			} else {
				onlineBtn.removeClass('btn-success').addClass('btn-danger').text('Offline');
			}
		}
		setTimeout(updateOnline, 1000);
	}

	o.ready = function () {
		onlineBtn = $('#onlineBtn');
		updateOnline();
		// test 

		if ($('#applicationcache').length) {
			play.appcache.init();
		} else if ($('#benchmark').length) {
			play.benchmark.init();
		}

		if ($('#benchmark-log').length) {
			$('#runBtn').on('click', function (event) {
				event.preventDefault();
				MAIN.runBenchmark();
				$('#benchmark-log').html('Starting...');
			});
		}

		$('.tooltipable').tooltip(); 
	};

	doc.ready(o.ready);
	return o;
})();