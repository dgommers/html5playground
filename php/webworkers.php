
<div class="row-fluid">
	<div class="span6">
	
		<h2>Web Workers <span class="label label-success">HTML5</span></h2>
		<p>A web worker is a JavaScript that runs in the background, independently of other scripts, without affecting the performance of the page. You can continue to do whatever you want: clicking, selecting things, etc., while the web worker runs in the background.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Offline available</li>
					<li><i class="icon-ok"></i> Key-value based</li
					<li><i class="icon-ok"></i> Local stored</li>
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> SQL Syntax</li>
					<li><i class="icon-remove"></i> Expiration date</li>
					<li><i class="icon-remove"></i> Server needed</li>
				</ul>
			</div>
		</div>
		
		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE8+">*</li>
		</ul>
		
		<hr>
	</div>
	<div class="span6">
		<h3>Performance</h3>
		<p>This example is quite simple: it launches n Web Workers (threads) and each one verifies a portion of the problem set.</p>
		<a id="runBtn" class="btn btn-primary">Run test</a>
		<pre class="well console" id="benchmark-log">

		</pre>
	<div>
</div>