
<div class="row-fluid">
	<div class="span6">
	
		<h2>LocalStorage <span class="label label-success">HTML5</span></h2>
		<p>Earlier, this was done with cookies. However, Web Storage is more secure and faster. The data is not included with every server request, but used ONLY when asked for. It is also possible to store large amounts of data, without affecting the website's performance.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Offline available</li>
					<li><i class="icon-ok"></i> Key-value based</li
					<li><i class="icon-ok"></i> Local stored</li>
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> SQL Syntax</li>
					<li><i class="icon-remove"></i> Expiration date</li>
					<li><i class="icon-remove"></i> Server needed</li>
				</ul>
			</div>
		</div>
		
		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE8+">*</li>
		</ul>
		
		<hr>
		
		<h2>SessionStorage <span class="label label-success">HTML5</span></h2>
		<p>The sessionStorage object is equal to the localStorage object, <strong>except</strong> that it stores the data for only one session. The data is deleted when the user closes the browser window.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Offline available</li>
					<li><i class="icon-ok"></i> Key-value based</li
					<li><i class="icon-ok"></i> Local stored</li>
					<li><i class="icon-ok"></i> Expiration date</li>
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> SQL Syntax</li>
					<li><i class="icon-remove"></i> Server needed</li>
				</ul>
			</div>
		</div>

		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE8+">*</li>
		</ul>
		
		<hr>

		<h2>IndexedDB <span class="label label-success">HTML5</span></h2>
		<p>IndexedDB is basically a simple flat-file database with hierarchical key/value persistence and basic indexing.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Offline available</li>
					<li><i class="icon-ok"></i> Local stored</li>
					<li><i class="icon-ok"></i> Transactions</li>
					<li><i class="icon-ok"></i> Key-value(/index) based</li>
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> SQL Syntax</li>
					<li><i class="icon-remove"></i> Server needed</li>
				</ul>
			</div>
		</div>

		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera false"></li>
			<li class="safari false"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE10+">*</li>
		</ul>
		
		<hr>

		<h2>WebSQL <span class="label label-success">HTML5</span></h2>
		<p>IndexedDB is basically a simple flat-file database with hierarchical key/value persistence and basic indexing.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Offline available</li>
					<li><i class="icon-ok"></i> Local stored</li>
					<li><i class="icon-ok"></i> Transactions</li>
					<li><i class="icon-ok"></i> SQL Syntax</li>
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> Server needed</li>
					<li><i class="icon-remove"></i> Allready deprecated</li>
					<li><i class="icon-remove"></i> Need permission</li>
				</ul>
			</div>
		</div>

		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox tooltipable" data-toggle="tooltip" data-original-title="Extension required">*</li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie false"></li>
		</ul>

		<hr> 

		<h2>Cookies <span class="label label-warning">HTML4</span></h2>
		<p>A cookie is a variable that is stored on the visitor's computer. Each time the same computer requests a page with a browser, it will send the cookie too. With JavaScript, you can both create and retrieve cookie values.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Offline available <em>(sort of)</em></li>
					<li><i class="icon-ok"></i> Key-value based</li
					<li><i class="icon-ok"></i> Local stored</li>
					<li><i class="icon-ok"></i> Expiration date</li>
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> SQL Syntax</li>
					<li><i class="icon-remove"></i> Server needed</li>
					<li><i class="icon-remove"></i> Max size of 4KB</li>
				</ul>
			</div>
		</div>

		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie"></li>
		</ul>
		
		<hr>
		
		<h2>Remote API <span class="label label-warning">HTML4</span></h2>
		<p>A cookie is a variable that is stored on the visitor's computer. Each time the same computer requests a page with a browser, it will send the cookie too. With JavaScript, you can both create and retrieve cookie values.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Key-value based</li
					<li><i class="icon-ok"></i> Server needed</li>
					<li><i class="icon-ok"></i> SQL Syntax <em>(sort of)</em></li>
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> Local stored</li>
					<li><i class="icon-remove"></i> Expiration date</li>
					<li><i class="icon-remove"></i> Offline available</li>
				</ul>
			</div>
		</div>

		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE8+"></li>
		</ul>
		
	</div>
	<div class="span6">
		<h3>Performance</h3>
		<p>In this test, every storage technology will create, read and remove an storage row of 512 bytes.</p>
		
		<div id="benchmark">
			<table cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Method</th>
						<th>Duration</th>
						<th>Speed</th>
						<th>MOE</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr data-test="localstorage">
						<td>LocalStorage</td>
						<td></td><td></td><td></td><td></td>
					</tr>
					<tr data-test="sessionstorage">
						<td>SessionStorage</td>
						<td></td><td></td><td></td><td></td>
					</tr>
					<tr data-test="cookie">
						<td>Cookie</td>
						<td></td><td></td><td></td><td></td>
					</tr>
					<tr data-test="websql">
						<td>WebSQL</td>
						<td></td><td></td><td></td><td></td>
					</tr>
					<tr data-test="remoteapi">
						<td>Remote API</td>
						<td></td><td></td><td></td><td></td>
					</tr>
					<tr data-test="indexeddb">
						<td>IndexedDB</td>
						<td></td><td></td><td></td><td></td>
					</tr>
				</tbody>
			</table>
			<a class="btn btn-large btn-primary">Start</a>
		</div>
	<div>
</div>