
<div class="row-fluid">
	<div class="span5">
		<h2>Application Cache <span class="label label-success">HTML5</span></h2> 
		<p>HTML5 introduces application cache, which means that a web application is cached, and accessible without an internet connection. Every page with the manifest attribute specified will be cached when the user visits it. If the manifest attribute is not specified, the page will not be cached (unless the page is specified directly in the manifest file).</p>
		<div class="row-fluid">
			<div class="span5">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Performance</li>
					<li><i class="icon-ok"></i> Reduced bandwidth</li>
					<li><i class="icon-ok"></i> Reduced server load</li>
					<li><i class="icon-ok"></i> Manage cache state</li>
					<li><i class="icon-ok"></i> Reliable</li>
				</ul>
			</div>
			<div class="span5">
				<ul class="unstyled">
					<li><i class="icon-remove"></i>  No manifest updates</li>
				</ul>
			</div>
		</div>

		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE10+">*</li>
		</ul>

		<hr>

		<h2>Caching with expire <span class="label label-warning">HTML4</span></h2> 
		<div class="row-fluid">
			<p>There is no really alternative for Application Cache in case of offline browsing and local applications. But there is a method to reduce bandwidth usage for such as images, js and other asset files. By adding a max-age or expire date header, the browser cache these as temporary internet files and will not request the server until the files are expired or the cache is cleared.</p>
			<div class="row-fluid">
				<div class="span5">
					<ul class="unstyled">
						<li><i class="icon-ok"></i> Performance</li>
						<li><i class="icon-ok"></i> Reduced bandwidth</li>
						<li><i class="icon-ok"></i> Reduced server load</li>
						<li><i class="icon-ok"></i>  No manifest updates</li>
					</ul>
				</div>
				<div class="span5">
					<ul class="unstyled">
						<li><i class="icon-remove"></i> Offline usage</li>
						<li><i class="icon-remove"></i> Manage cache state</li>
						<li><i class="icon-remove"></i> Reliable</li>
					</ul>
				</div>
			</div>
		</div>

		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE10+">*</li>
		</ul>
	</div>
	<div class="span3">
		<h3>Current status</h3>
		<div id="appcache">
			<a id="updateBtn" class="btn btn-primary">Check for new cache</a>
			<a id="reloadBtn" class="btn disabled">Reload page</a>
			<pre id="console" class="console well"></pre>
		</div>
	</div>
	<div class="span4">
		<h3>Manifest</h3>
		<pre id="file"><?php require 'appcache.php'; ?></pre>
	</div>

</div>