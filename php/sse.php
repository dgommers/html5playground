<div class="row">

	<div class="span6">
		<h2>Server-sent events</h2>
		<p>A server-sent event is when a web page automatically gets updates from a server.

This was also possible before, but the web page would have to ask if any updates were available. With server-sent events, the updates come automatically.</p>
		<hr>

		<h2>EventSource</h2>
		<img src="lib/img/eventsource.png" alt="" width="75%" height="75%"><br><br>
		<p>A server-sent which is only send when there is new information available. This is possible because the client and server are always connected.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Realtime trafic</li>
					<li><i class="icon-ok"></i> No reopening connections</li
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> Unused connetions</li>
					<li><i class="icon-remove"></i> One way traffic</li>
				</ul>
			</div>
		</div>
		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie false"></li>
		</ul>
		<hr>
		
		<h2>Long Polling</h2>
		<img src="lib/img/longpolling.png" alt="" width="75%" height="75%"><br><br>
		<p>A server-sent which is only send within a time interval. If there is no new information available within the time interval the connection will be closed.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Realtime trafic</li>
					<li><i class="icon-ok"></i> Two way traffic</li
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> Reopening connections</li>
				</ul>
			</div>
		</div>
		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE10+">*</li>
		</ul>
		<hr>
		
		<h2>Websockets</h2>
		<img src="lib/img/websockets.png" alt="" width="75%" height="75%"><br><br>
		<p>A server-sent which is always open and enables traffic both ways.</p>
		<div class="row-fluid">
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-ok"></i> Realtime trafic</li>
					<li><i class="icon-ok"></i> Two way traffic</li
				</ul>
			</div>
			<div class="span4">
				<ul class="unstyled">
					<li><i class="icon-remove"></i> Difficult to setup</li>
				</ul>
			</div>
		</div>
		<ul class="browser unstyled">
			<li class="chrome"></li>
			<li class="firefox"></li>
			<li class="opera"></li>
			<li class="safari"></li>
			<li class="ie tooltipable" data-toggle="tooltip" data-original-title="Only IE10+">*</li>
		</ul>
		<hr>
	</div>
	<div class="span6">
		<h3>Example</h3>
		<p>In this test, the server is sending the current date and time using a server-sent event.</p>
		<pre id="console" class="console well"></pre>
	<div>
	<script>
		if(typeof(EventSource)!=="undefined")
		{
		var source = new EventSource("php/sse-server.php");
		source.onmessage = function(event){
			document.getElementById("console").innerHTML+=event.data+"\n";
		};
		} else {
			document.getElementById("console").innerHTML="Sorry, your browser does not support server-sent events...";
		}
	</script>

</body>
</html>
