<?php

if (!isset($m)) {
	header('Content-Type: text/cache-manifest');
}

echo "CACHE MANIFEST\n\nCACHE:\n";

$hashes = "";
$network = array(
	'./php/sse-server.php'
);
$networks = "";

$dir = new RecursiveDirectoryIterator(".");
foreach(new RecursiveIteratorIterator($dir) as $file) {
  if ($file->IsFile() &&
      $file != "./any.appcache" &&
      substr($file->getFilename(), 0, 1) != "." && !in_array($file, $network) && substr($file, 0, 6) != './.git')
  {
    echo $file . "\n";
    $hashes .= md5_file($file);
  }
}

echo "\n\nNETWORK:\n".join($network, "\n");



echo "\n\n# Hash: " . md5($hashes) . "\n";
	
?>